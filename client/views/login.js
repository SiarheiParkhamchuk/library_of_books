import {JetView} from "webix-jet";
import SignInView from "../views/login/sign_in";
import SignUpView from "../views/login/sign_up";

export default class LoginView extends JetView{
	config(){

		const tabView = {
			view:"tabview",
			cells:[
				{
					header:"Sign In",
					body:SignInView
				},
				{
					header:"Sign Up",
					body:SignUpView
				}
			]
		};

		return {
			cols:[{}, { rows:[{}, tabView, {}]}, {}]
		};
	}

	init(view){

	}

}