import {JetView} from "webix-jet";

webix.protoUI({
	name:"formOverlay"
}, webix.ui.form, webix.OverlayBox);

export default class SignUpView extends JetView{
	config(){
		const elements = [
			{
				cols:[
					{
						rows:[
							{
								view:"text",
								name:"first_name",
								label:"First name",
								localId:"form:first_name",
								required:true,
								invalidMessage:"First name cant't be empty"
							},
							{
								view:"text",
								name:"middle_name",
								label:"Middle name",
							},
							{
								view:"text",
								name:"last_name",
								label:"Last name",
								required:true,
								invalidMessage:"Last name cant't be empty"
							},
							{
								view:"text",
								name:"passport_num",
								label:"Passport number",
								required:true,
								invalidMessage:"Passport number cant't be empty"
							},
							{
								view:"datepicker",
								name:"birthday",
								label:"Birthday",
							},
							{
								view:"text",
								name:"address",
								label:"Address",
							},
						]
					},
					{width:20},
					{
						rows:[
							{
								view:"text",
								name:"phone[0]",
								label:"Phone",
								required:true,
								relatedView:"$phone_one",
								relatedAction:"show",
								invalidMessage:"Phone cant't be empty"
							},
							{
								view:"text",
								name:"phone[1]",
								label:"Phone",
								id:"$phone_one",
								hidden:"true",
								relatedView:"$phone_two",
								relatedAction:"show",
							},
							{
								view:"text",
								name:"phone[2]",
								label:"Phone",
								id:"$phone_two",
								hidden:"true",
								relatedView:"$phone_three",
								relatedAction:"show",
							},
							{
								view:"text",
								name:"phone[3]",
								label:"Phone",
								id:"$phone_three",
								hidden:"true"
							},
							{
								view:"text",
								name:"email",
								label:"Email",
								required:true,
								invalidMessage:"Email cant't be empty"
							},
							{
								view:"text",
								name:"password",
								label:"Password",
								type:"password",
								localId:"password",
								required:true,
								invalidMessage:"Password cant't be empty"
							},
							{
								view:"text",
								name:"confirmPassword",
								label:"Confirm password",
								type:"password",
								localId:"confirmPassword",
								invalidMessage:"Passwords doesn't match"
							},
						]
					}
				]
			},
			{
				cols:[
					{},
					{
						view:"button",
						label:"Sign up",
						localId:"signUpButton",
						hotkey:"enter"
					}
				]
			}
		];

		return {
			view:"formOverlay",
			width: 800,
			localId:"signUpForm",
			elementsConfig:{
				labelWidth: 140
			},
			elements:elements,
			rules:{
				first_name:webix.rules.isNotEmpty,
				last_name:webix.rules.isNotEmpty,
				passport_num:webix.rules.isNotEmpty,
				"phone[0]":webix.rules.isNotEmpty,
				email:webix.rules.isNotEmpty,
				password:webix.rules.isNotEmpty,
				confirmPassword: (data) => {
					const passValue = this.$$("password").getValue();
					return passValue.trim() === data.trim();
				}
			}
		};
	}

	init(view){
		const form = this.$$("signUpForm");
		const signUpButton = this.$$("signUpButton");
		const confirmPassword = this.$$("confirmPassword");
		const password = this.$$("password");
		const messages = {
			error:"Passwords doesn't match!",
			success: "Passwords match."
		};

		this.on(confirmPassword, "onBlur", () => { this.checkMathPasswords(confirmPassword, password, messages); });

		this.on(password, "onBlur", () => { this.checkMathPasswords(confirmPassword, password, messages); });

		this.on(signUpButton, "onItemClick", () => {
			if ( !form.validate() ) return false;
			const values = form.getValues({hidden:false});

			for ( let key in values ){
				if ( !values.hasOwnProperty(key) ) continue;

				if ( key.indexOf("phone") !== -1 && values[key] === "" ) {
					delete values[key];
				}
			}

			form.showOverlay("<h1 class='form-overlay'>Saving Data...</h1>");
			signUpButton.disable();

			webix.ajax().post("/server/sign_up", values, {
				error:function(text){
					let errors;

					try {
						errors = JSON.parse(text);
					} catch(e){
						errors = text;
					}

					if ( Array.isArray(errors) && errors.length > 0 ) {
						errors.forEach(item => {
							webix.message({type: "error", text: item.msg});
						})
					} else{
						webix.message({type: "error", text: text});
					}

					form.hideOverlay();
					signUpButton.enable();
				},
				success:function(){
					webix.message({type: "info", text: "Data successfully saved."});
					form.hideOverlay();
					signUpButton.enable();
					form.clear();
					form.clearValidation();
				}
			});
		});
	}



	checkMathPasswords(field1, field2, message){
		const value1 = field1.getValue();
		const value2 = field2.getValue();

		if ( !(value1.trim() === value2.trim()) ) {
			webix.message({type:"error", text:message.error});
			return false;
		}

		webix.message({type:"info", text:message.success});
	}
}