import {JetView} from "webix-jet";

export default class SignInView extends JetView{
	config(){
		return {
			view:"form",
			localId:"login:form",
			width:400,
			borderless:false,
			margin:10,
			rows:[
				{
					type:"header",
					template: "Library of books"
				},
				{
					view:"text",
					name:"email",
					label:"User Name",
					labelPosition:"top",
					localId: "form:email"
				},
				{
					view:"text",
					type:"password",
					name:"password",
					label:"Password",
					labelPosition:"top"
				},
				{
					view:"button",
					value:"Login",
					click:() => this.do_login(), hotkey:"enter" }
			],
			rules:{
				email:webix.rules.isNotEmpty,
				password:webix.rules.isNotEmpty
			}
		};
	}

	init(view){

	}

	do_login(){
		const user = this.app.getService("user");
		const form = this.$$("login:form");

		if (form.validate()){
			const data = form.getValues();

			user.login(data.email, data.password).catch(function(){
				webix.html.removeCss(form.$view, "invalid_login");
				form.elements.password.focus();
				webix.delay(function(){
					webix.html.addCss(form.$view, "invalid_login");
				});
			});
		}
	}
}