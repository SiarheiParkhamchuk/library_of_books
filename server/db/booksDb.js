const mysql = require("mysql");
const db = require("../config/").db;

module.exports = mysql.createPool({
	port            : db.port,
	connectionLimit : db.connectionLimit,
	host            : db.host,
	user            : db.user,
	password        : db.password,
	database        : db.database,
	multipleStatements: db.multipleStatements
});