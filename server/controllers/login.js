const Login = require("../modeles/login");
const bcrypt = require("bcryptjs");


function hashPassword(pass, round){
	const salt = bcrypt.genSaltSync(round);
	const hash = bcrypt.hashSync(pass, salt);
	return {
		salt: salt,
		hash: hash
	};
}

exports.addUser = (body) => {
	const hashData = hashPassword(body.password, 5);
	body.password = hashData.hash;
	body.salt = hashData.salt;

	delete body.confirmPassword;
	let data = {};
	if ( body.phone ) {
		data.phone = body.phone;
		delete body.phone;
	}
	data.body = body;

	return Login.addUser(data);
};

exports.checkUserByPassport = (data) => {
	return Login.checkUserByPassport(data);
};

exports.checkUserByMail = (data) => {
	return Login.checkUserByMail(data);
};

exports.checkUserById = (data) => {
	return Login.checkUserById(data);
};
