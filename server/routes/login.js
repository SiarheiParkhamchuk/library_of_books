const usersController = require("../controllers/login");
const validator = require("../middlewares/validation");

module.exports = function(app, passport){

	app.post("/server/login", (req, res, next) => {
		passport.authenticate("local-signin", function(err, user, info) {
			if ( err ) {
				return next(err);
			}

			if ( !user ) {
				return res.status(403).send(info.message );
			}

			req.logIn(user, function(err) {
				if (err) {
					return next(err);
				}
				return res.send(user);
			});
		})(req, res, next);
	});

	app.post("/server/login/status", (req, res) => {
		if (req.isAuthenticated()){
			res.send(req.session.passport).end();
		} else {
			res.send(null);
		}
	});

	app.post("/server/logout", (req, res) => {
		req.logout();
		res.send(null);
	});

	app.post("/server/sign_up", validator.checks, (req, res) => {

		req.getValidationResult()
			.then( errors => {

				if ( !errors.isEmpty() ) {
					res.status(400).json(errors.array());
				} else {
					usersController.addUser(req.body)
						.then( rows => {
							res.json(rows);
						} )
						.catch( err => {
							switch(err.code){
								case "ER_DUP_ENTRY":
									res.status(422).json( {"msg":"This user already exist!"} );
									break;
								case "ER_DATA_TOO_LONG":
									res.status(422).json( {"msg":"Data is too long!"} );
									break;
								default:
									res.status(500).json( {"msg":"Unknown error!"} );
									throw err;
							}
						} );
				}
			} ).catch( error => {
				throw error;
			});
	});
};