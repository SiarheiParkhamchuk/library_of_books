const express = require("express");
const config = require("./config/local");
const passport = require("passport");
const User = require("./controllers/login");

const app = express();

require("./middlewares/index")(app);
require("./middlewares/passport")(passport, User);
require("./routes/login")(app, passport);

app.listen(config.app.port, () => console.log(`LISTENING ON PORT ${config.app.port}!`));