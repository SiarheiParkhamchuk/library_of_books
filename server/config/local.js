const config = {
	app: {
		port: 3000,
	},
	db:{
		multipleStatements: true
	}
};

module.exports = config;