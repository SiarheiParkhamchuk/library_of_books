const config = {
	app: {
		port: 3000,
		host:"localhost"
	},
	db:{
	    port: 3306,
		host: "127.0.0.1",
		user:"root",
		password:"12345",
		database:"books",
		multipleStatements: false,
		pool:{
			connectionLimit: 10
		}
	}
};

module.exports = config;