const session = require("express-session");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const MySQLStore = require("express-mysql-session")(session);
const pool = require("../db/booksDb");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");


module.exports = function(app){
	let sessionStore = new MySQLStore({}, pool);

	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(expressValidator());

	app.use(cookieParser());

	app.use((req, res, next) => {
		res.set({
			"Access-Control-Allow-Origin" : "*",
			"Content-Type" : "application/json"
		});
		res.status(200);
		next();
	});

	app.use(session({
		secret: "a little bunny",
		resave: false,
		saveUninitialized: true,
		store: sessionStore,
	}));

	app.use(passport.initialize());
	app.use(passport.session());
};