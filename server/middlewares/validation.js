const { check } = require("express-validator/check");
const { sanitizeBody  } = require("express-validator/filter");
const usersController = require("../controllers/login");

module.exports = {
	checks: [
		sanitizeBody ("*").escape().trim(),

		check("first_name")
			.isLength({ min: 2 }).withMessage("First name is required. Minimum 2 char.")
			.isLength({ max: 45 }).withMessage("Maximum first name length 45 characters.")
			.escape(),

		check("middle_name")
			.isLength({ max:45 }).withMessage("Maximum middle name length 45 characters.")
			.escape(),

		check("last_name")
			.isLength({ min: 2 }).withMessage("Last name is required. Minimum 2 char.")
			.isLength({ max: 45 }).withMessage("Maximum last name length 45 characters.")
			.escape(),

		check("passport_num")
			.not().isEmpty().withMessage("Passport number is a required field.")
			.isLength({ max:15 }).withMessage("Maximum passport number length 15 characters.")
			.isAlphanumeric().withMessage("Passport number must be alphanumeric.")
			.custom((value, {req}) => {
				return usersController.checkUserByPassport(req.body)
					.then(user => {
						if ( user.length > 0 ) {
							return Promise.reject("User already exist");
						}
					})
					.catch( err => {throw err;});
			}),

		check("birthday")
			.isLength({ max:15 }).withMessage("Maximum birthday length 25 characters"),

		check("address")
			.isLength({ max:180 }).withMessage("Maximum address length 180 characters"),

		check("phone")
			.isArray()
			.custom(value => {

				if ( !value || value.length === 0 ) throw new Error("You must specify at least one phone");
				for ( let i = 0; i < value.length; i+=1 ){
					if ( value[i].length > 20 ) {
						throw new Error("Maximum phone number length 20 characters");
					}
				}
				return true;
			}),

		check("email")
			.isEmail().withMessage("Mail is not correct.")
			.isLength({ min: 5 }).withMessage("Email is required. Minimum 5 char.")
			.isLength({ max: 45 }).withMessage("Maximum email length 60 characters.")
			.custom((value, {req}) => {
				return usersController.checkUserByMail(req.body)
					.then(user => {
						if ( user.length > 0 ) {
							return Promise.reject("User already exist");
						}
					})
					.catch( err => {throw err;});
			}),

		check("password")
			.isLength({ min:8 }).withMessage("Password must be at least 8 characters in length.")
			.matches("[0-9]").withMessage("Password must contain at least 1 number.")
			.matches("[a-z]").withMessage("Password must contain at least 1 lowercase letter.")
			.matches("[A-Z]").withMessage("Password must contain at least 1 uppercase letter.")
			.custom((value, {req}) => {
				if (value !== req.body.confirmPassword) {
					return false;
				} else {
					return value;
				}
			}).withMessage("Passwords doesn't match."),
	],
};