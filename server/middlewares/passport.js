const bcrypt = require("bcryptjs");

module.exports = function(passport, user){

	const User = user;
	let LocalStrategy = require("passport-local").Strategy;

	passport.serializeUser( (user, done) => {
		done(null, user.id);
	});

	passport.deserializeUser( (id, done) => {
		User.checkUserById({id:id})
			.then( user => {
				if ( user.length !== 0 ){
					done(null, user);
				} else {
					done(user.error, false);
				}
			})
			.catch ( err  => done(err, false));
	});

	passport.use("local-signin", new LocalStrategy({
		usernameField: "user",
		passwordField: "pass",
		passReqToCallback : true
	}, (req, email, password, done) => {

		function confirmPassword(pass, hash) {
			return 	bcrypt.compareSync(pass, hash);
		}

		User.checkUserByMail({email: email})
			.then( user => {
				if ( user.length === 0 ) {
					return done(null, false, {message:"User not found."});
				}

				if ( !confirmPassword(password, user[0].password) ) {
					return done(null, false, {message:"Incorrect email or password"});
				}

				return done(null, {id: user[0].id});
			} )
			.catch( err => {
				return done(err, false, { message: "Something went wrong." });
			} );

	} ));
};