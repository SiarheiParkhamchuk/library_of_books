const pool = require("../db/booksDb");

class Login {

	static _queryDb(sql, props){
		return new Promise( (resolve, reject) => {
			pool.getConnection((err, connection) => {
				if ( err ) {
					reject(err);
				}

				connection.query(sql, props, (err, body) => {
					if ( err ) {
						reject(err);
					} else {
						resolve(body);
					}
				});

				connection.on("error", (err) => {
					reject(err);
				});

				connection.release();
			});
		});
	}

	static addUser(data){
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if ( err ) { reject(err); }

				connection.beginTransaction(function(err) {
					if ( err ) { reject(err); }

					connection.query("INSERT INTO readers SET ?", data.body, function (err, results) {
						if (err) {
							return connection.rollback(function() {
								reject(err);
							});
						}

						let values = "";
						for ( let i = 0; i < data.phone.length; i+=1 ){
							values += `(${results.insertId}, '${data.phone[i]}')`;
							if ( i < data.phone.length - 1 ) {
								values += ",";
							}
						}

						connection.query(`INSERT INTO phones (id_reader, phone) VALUES ${values}`, function (error, results) {
							if (error) {
								return connection.rollback(function() {
									reject(error);
								});
							}
							connection.commit(function(err) {
								if (err) {
									return connection.rollback(function() {
										reject(err);
									});
								}
								resolve({id:results.insertId});
							});
						});
					});
				});
			});
		});
	}

	static checkUserByMail(data){
		return this._queryDb("SELECT * FROM readers WHERE email=?", data.email);
	}

	static checkUserByPassport(data){
		return this._queryDb("SELECT * FROM readers WHERE passport_num=?", data.passport_num);
	}

	static checkUserById(data){
		return this._queryDb("SELECT * FROM readers WHERE id=?", data.id);
	}
}

module.exports = Login;